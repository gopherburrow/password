package main

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"gitlab.com/gopherburrow/ms"

	_ "github.com/lib/pq"
)

const (
	defaultPrepareDatabaseTimeoutsInMs = 500
)

const (
	createTableSQL = `
CREATE TABLE
	Passwords (
		resource_peppered_hash BYTEA NOT NULL ,
		password_salted_hash BYTEA NOT NULL,
		password_salt BYTEA NOT NULL,
		CONSTRAINT Passwords_PK PRIMARY KEY (resource_peppered_hash)
	);

ALTER TABLE Passwords ALTER COLUMN resource_peppered_hash SET STORAGE PLAIN;
ALTER TABLE Passwords ALTER COLUMN password_salted_hash SET STORAGE PLAIN;
ALTER TABLE Passwords ALTER COLUMN password_salt SET STORAGE PLAIN;

CREATE TABLE
	Recovery_Tokens (
		resource_peppered_hash BYTEA NOT NULL,
		recovery_token_salted_hash BYTEA NULL,
		recovery_token_salt BYTEA NULL,
		CONSTRAINT Recovery_Tokens_PK PRIMARY KEY (resource_peppered_hash),
		CONSTRAINT Recovery_Tokens_FK_Passwords FOREIGN KEY (resource_peppered_hash) REFERENCES Passwords(resource_peppered_hash),
		CONSTRAINT Recovery_Tokens_UQ_recovery_token_salted_hash UNIQUE (recovery_token_salted_hash)
	);

ALTER TABLE Recovery_Tokens ALTER COLUMN resource_peppered_hash SET STORAGE PLAIN;
ALTER TABLE Recovery_Tokens ALTER COLUMN recovery_token_salted_hash SET STORAGE PLAIN;
ALTER TABLE Recovery_Tokens ALTER COLUMN recovery_token_salt SET STORAGE PLAIN;
`
	insertPasswordSQL = `
INSERT INTO 
	Passwords (
		resource_peppered_hash,
		password_salted_hash,
		password_salt
	)
	VALUES (
		$1, -- resource_peppered_hash
		$2, -- password_salted_hash
		$3  -- password_salt
	);
`

	selectPasswordRowSQL = `
SELECT 
	resource_peppered_hash,
	password_salted_hash,
	password_salt
FROM
	Passwords
WHERE
	resource_peppered_hash = $1;
`

	selectPasswordExistsSQL = `
SELECT 
	TRUE
FROM
	Passwords
WHERE
	resource_peppered_hash = $1;
`

	updatePasswordRowSQL = `
UPDATE Passwords SET
	password_salted_hash = $2,
	password_salt = $3
WHERE
	resource_peppered_hash = $1;
`

	deletePasswordRowSQL = `
DELETE FROM
	Passwords
WHERE
	resource_peppered_hash = $1;
`

	insertRecoveryTokenSQL = `
INSERT INTO 
	Recovery_Tokens (
		resource_peppered_hash,
		recovery_token_salted_hash,
		recovery_token_salt
	)
	VALUES (
		$1, -- resource_peppered_hash
		$2, -- recovery_token_salted_hash
		$3  -- recovery_token_salt
	);
`

	selectRecoveryTokenRowSQL = `
SELECT 
	resource_peppered_hash,
	recovery_token_salted_hash,
	recovery_token_salt
FROM
	Recovery_Tokens
WHERE
	resource_peppered_hash = $1;
`

	deleteRecoveryTokenRowSQL = `
DELETE FROM
	Recovery_Tokens
WHERE
	resource_peppered_hash = $1;
`
)

var (
	errRowsAffectedNotUnique = errors.New("passwords: Rows affected not unique")
)

var (
	db                       *sql.DB
	insertPasswordStmt       *sql.Stmt
	selectPasswordRowStmt    *sql.Stmt
	selectPasswordExistsStmt *sql.Stmt
	updatePasswordRowStmt    *sql.Stmt
	deletePasswordRowStmt    *sql.Stmt
)

func passwordRowReader() (interface{}, []interface{}) {
	dbEnt := &passwordEntity{}
	return dbEnt, passwordScanArgs(dbEnt)
}

func passwordScanArgs(dbEnt *passwordEntity) []interface{} {
	return []interface{}{&dbEnt.resourcePepperedHash, &dbEnt.passwordSaltedHash, &dbEnt.passwordSalt}
}

// func recoveryTokenRowReader() (interface{}, []interface{}) {
// 	dbEnt := &recoveryTokenEntity{}
// 	return dbEnt, recoveryTokenScanArgs(dbEnt)
// }

// func recoveryTokenScanArgs(dbEnt *recoveryTokenEntity) []interface{} {
// 	return []interface{}{&dbEnt.resourcePepperedHash, &dbEnt.recoveryTokenSaltedHash, &dbEnt.recoveryTokenSalt}
// }

func initPersistence() error {
	ctxRoot := context.Background()
	ctx, cancel := context.WithTimeout(ctxRoot, defaultPrepareDatabaseTimeoutsInMs*time.Millisecond)
	defer cancel()

	var err error

	if db, err = sql.Open("postgres", "postgresql://passwords:passwords@localhost:5432/passwords"); err != nil {
		return err
	}

	if err := ms.PrepareContextQueriesStmtPairs(
		ctx,
		db,
		insertPasswordSQL, &insertPasswordStmt,
		selectPasswordRowSQL, &selectPasswordRowStmt,
		selectPasswordExistsSQL, &selectPasswordExistsStmt,
		updatePasswordRowSQL, &updatePasswordRowStmt,
		deletePasswordRowSQL, &deletePasswordRowStmt,
	); err != nil {
		return err
	}

	return nil
}

func insertPassword(ctx context.Context, tx *sql.Tx, e *passwordEntity) error {
	return ms.Insert(ctx, tx, insertPasswordStmt, passwordScanArgs(e)...)
}

func selectPasswordRow(ctx context.Context, tx *sql.Tx, resource []byte) (*passwordEntity, error) {
	tbci, err := ms.SelectRow(ctx, tx, selectPasswordRowStmt, passwordRowReader, resource)
	if err != nil {
		return nil, err
	}
	return tbci.(*passwordEntity), nil
}

func selectPasswordExists(ctx context.Context, tx *sql.Tx, resource []byte) (bool, error) {
	return ms.SelectExists(ctx, tx, selectPasswordExistsStmt, resource)
}

func updatePasswordRow(ctx context.Context, tx *sql.Tx, e *passwordEntity) error {
	return ms.UpdateRow(ctx, tx, updatePasswordRowStmt, passwordScanArgs(e)...)
}

func deletePasswordRow(ctx context.Context, tx *sql.Tx, resource []byte) error {
	return ms.DeleteRow(ctx, tx, deletePasswordRowStmt, resource)
}
