package main

import (
	"log"
	"net/http"

	"gitlab.com/gopherburrow/ms"
	"gitlab.com/gopherburrow/mux"
)

var (
	m = &mux.Mux{}
)

type putPasswordReqBody struct {
	Password string `json:"password"`
}

type postPasswordAutenticationsReqBody struct {
	Password string `json:"password"`
}

type postPasswordChangesReqBody struct {
	OldPassword string `json:"oldPassword"`
	NewPassword string `json:"newPassword"`
}

// type postPasswordRecoveryTokensRespBody struct {
// 	RecoveryToken string `json:"recoveryToken"`
// }

// type putPasswordRecoveryTokenRecoveryReqBody struct {
// 	NewPassword string `json:"newPassword"`
// }

func initView() error {
	if err := ms.PrepareMux(m,
		"http://localhost:8080/api/passwords/{resource}", http.MethodPut, putPassword,
		"http://localhost:8080/api/passwords/{resource}", http.MethodDelete, deletePassword,
		"http://localhost:8080/api/passwords/{resource}/authentications", http.MethodPost, postPasswordsAuthentications,
		"http://localhost:8080/api/passwords/{resource}/changes", http.MethodPost, postPasswordsChanges,
	); err != nil {
		return err
	}
	log.Fatal(http.ListenAndServe(":8080", m))
	return nil
}

func putPassword(w http.ResponseWriter, r *http.Request) {
	resource, body := "", putPasswordReqBody{}
	ms.Service(w, r,
		ms.Values(&resource, &body),
		ms.Errors{
			errPasswordAlreadyExists: ms.Error{Code: "errPasswordAlreadyExists", Status: http.StatusConflict},
		},
		func() (interface{}, error) {
			return nil, createPassword(r.Context(), resource, body.Password)
		},
	)
}

func deletePassword(w http.ResponseWriter, r *http.Request) {
	resource := ""
	ms.Service(w, r,
		ms.Values(&resource),
		ms.Errors{
			errResourceNotFound: ms.Error{Code: "errResourceNotFound", Status: http.StatusNotFound},
		},
		func() (interface{}, error) {
			return nil, removePassword(r.Context(), resource)
		},
	)
}

func postPasswordsAuthentications(w http.ResponseWriter, r *http.Request) {
	resource, body := "", postPasswordAutenticationsReqBody{}
	ms.Service(w, r,
		ms.Values(&resource, &body),
		ms.Errors{
			errPasswordMismatch: ms.Error{Code: "errPasswordMismatch", Status: http.StatusForbidden},
		},
		func() (interface{}, error) {
			return nil, createPasswordAuthentication(r.Context(), resource, body.Password)
		},
	)
}

func postPasswordsChanges(w http.ResponseWriter, r *http.Request) {
	resource, body := "", postPasswordChangesReqBody{}
	ms.Service(w, r,
		ms.Values(&resource, &body),
		ms.Errors{
			errPasswordMismatch: ms.Error{Code: "errPasswordMismatch", Status: http.StatusForbidden},
		},
		func() (interface{}, error) {
			return nil, createPasswordChange(r.Context(), resource, body.OldPassword, body.NewPassword)
		},
	)
}

// func postPasswordsRecoveryTokens(w http.ResponseWriter, r *http.Request) {
// 	resource := ""
// 	ms.Service(w, r,
// 		ms.Values(&resource),
// 		ms.Errors{
// 			errResourceNotFound: ms.Error{Code: "errResourceNotFound", Status: http.StatusNotFound},
// 		},
// 		func() (interface{}, error) {
// 			token, err := createRecoveryToken(r.Context(), resource)
// 			if err != nil {
// 				return nil, err
// 			}
// 			return &postPasswordRecoveryTokensRespBody{
// 				RecoveryToken: base64.RawURLEncoding.EncodeToString(token),
// 			}, nil
// 		},
// 	)
// }

// func putPasswordsRecoveryTokensRecovery(w http.ResponseWriter, r *http.Request) {
// 	resource, recoveryToken, sort, body := "", []byte{}, "", putPasswordRecoveryTokenRecoveryReqBody{}

// 	ms.Service(w, r,
// 		ms.Values(&resource, &recoveryToken, "sort", &sort, &body),
// 		ms.Errors{
// 			errResourceNotFound:      ms.Error{Code: "errResourceNotFound", Status: http.StatusNotFound},
// 			errRecoveryTokenMismatch: ms.Error{Code: "errRecoveryTokenMismatch", Status: http.StatusForbidden},
// 		},
// 		func() (interface{}, error) {
// 			return nil, createRecoveryTokenRecovery(r.Context(), resource, recoveryToken, body.NewPassword)
// 		},
// 	)
// }
