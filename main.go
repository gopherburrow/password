package main

import (
	"log"
)

func main() {
	if err := initService(); err != nil {
		log.Fatal(err)
		return
	}
	if err := initPersistence(); err != nil {
		log.Fatal(err)
		return
	}
	if err := initView(); err != nil {
		log.Fatal(err)
		return
	}

}
