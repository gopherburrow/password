package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"crypto/sha512"
	"database/sql"
	"encoding/hex"
	"errors"
	"time"

	"gitlab.com/gopherburrow/ms"
)

var (
	errResourceNotFound      = errors.New("password: Resource not found")
	errPasswordAlreadyExists = errors.New("password: Password already exists")
	errPasswordMismatch      = errors.New("password: Password mismatch")
	errRecoveryTokenMismatch = errors.New("password: Recovery token mismatch")
)

const (
	pepperExample string = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF"
)

var (
	pepper []byte
)

const defaultTransactionTimeoutsInMs = 1000000

func initService() error {
	var err error
	pepper, err = hex.DecodeString(pepperExample)
	if err != nil {
		return err
	}
	return nil
}

func createPassword(ctx context.Context, resource string, password string) error {
	//TODO Validate

	resPepperHash := seasonStringHash(pepper, resource)

	return ms.OnTx(ctx, defaultTransactionTimeoutsInMs*time.Millisecond, db, sql.LevelReadCommitted, false, func(tx *sql.Tx) error {
		exists, err := selectPasswordExists(ctx, tx, resPepperHash)
		if err != nil {
			return err
		}

		if exists {
			return errPasswordAlreadyExists
		}

		pwdSalt := make([]byte, 64)
		if _, err := rand.Read(pwdSalt); err != nil {
			return err
		}
		pwdSaltHash := seasonStringHash(pwdSalt, password)

		dbEnt := &passwordEntity{
			resourcePepperedHash: resPepperHash,
			passwordSaltedHash:   pwdSaltHash,
			passwordSalt:         pwdSalt,
		}

		return insertPassword(ctx, tx, dbEnt)
	})
}

func removePassword(ctx context.Context, resource string) error {
	//TODO Validate

	resPepperHash := seasonStringHash(pepper, resource)

	return ms.OnTx(ctx, defaultTransactionTimeoutsInMs*time.Millisecond, db, sql.LevelReadCommitted, false, func(tx *sql.Tx) error {
		exists, err := selectPasswordExists(ctx, tx, resPepperHash)
		if err != nil {
			return err
		}

		if !exists {
			return errResourceNotFound
		}

		return deletePasswordRow(ctx, tx, resPepperHash)
	})
}

func createPasswordAuthentication(ctx context.Context, resource, password string) error {
	//TODO Validation

	resPepperHash := seasonStringHash(pepper, resource)

	tbci, err := ms.GetOnTx(ctx, defaultTransactionTimeoutsInMs*time.Millisecond, db, sql.LevelReadUncommitted, true, func(tx *sql.Tx) (model interface{}, err error) {
		return selectPasswordRow(ctx, tx, resPepperHash)
	})
	if err == sql.ErrNoRows {
		return errPasswordMismatch
	}
	if err != nil {
		return err
	}

	dbEnt, ok := tbci.(*passwordEntity)
	if !ok {
		return errors.New("password: TODO error")
	}
	pwdSaltHash := seasonStringHash(dbEnt.passwordSalt, password)
	if !bytes.Equal(dbEnt.passwordSaltedHash, pwdSaltHash) {
		return errPasswordMismatch
	}
	return nil
}

func createPasswordChange(ctx context.Context, resource, oldPassword, newPassword string) error {
	//TODO Validation

	resPepperHash := seasonStringHash(pepper, resource)

	newPwdSalt := make([]byte, 64)
	if _, err := rand.Read(newPwdSalt); err != nil {
		return err
	}
	newPwdSaltHash := seasonStringHash(newPwdSalt, newPassword)

	if err := ms.OnTx(ctx, defaultTransactionTimeoutsInMs*time.Millisecond, db, sql.LevelReadUncommitted, true, func(tx *sql.Tx) error {
		dbEnt, err := selectPasswordRow(ctx, tx, resPepperHash)
		if err == sql.ErrNoRows {
			return errPasswordMismatch
		}
		if err != nil {
			return err
		}

		oldPwdSaltHash := seasonStringHash(dbEnt.passwordSalt, oldPassword)
		if !bytes.Equal(dbEnt.passwordSaltedHash, oldPwdSaltHash) {
			return errPasswordMismatch
		}

		dbEnt = &passwordEntity{
			resourcePepperedHash: dbEnt.resourcePepperedHash,
			passwordSaltedHash:   newPwdSaltHash,
			passwordSalt:         newPwdSalt,
		}

		if err := updatePasswordRow(ctx, tx, dbEnt); err != nil {
			return err
		}
		return nil
	}); err != nil {
		return err
	}
	//TODO Save New Password and create new Salt
	return nil
}

// func createRecoveryToken(ctx context.Context, resource string) ([]byte, error) {
// 	resPepperHash := seasonStringHash(pepper, resource)

// 	//FIXME
// 	tbci, err := ms.GetOnTx(ctx, defaultTransactionTimeoutsInMs*time.Millisecond, db, sql.LevelReadCommitted, false, func(tx *sql.Tx) (interface{}, error) {
// 		pwdDbEnt, err := selectPasswordRow(ctx, tx, resPepperHash)
// 		if err == sql.ErrNoRows {
// 			return nil, errResourceNotFound
// 		}
// 		if err != nil {
// 			return nil, err
// 		}

// 		newRcvrSalt := make([]byte, 64)
// 		if _, err := rand.Read(newRcvrSalt); err != nil {
// 			return nil, err
// 		}

// 		newRcvrToken := make([]byte, 64)
// 		if _, err := rand.Read(newRcvrToken); err != nil {
// 			return nil, err
// 		}

// 		newRcvrTokenSaltHash := seasonHash(newRcvrSalt, newRcvrToken)

// 		dbEnt.recoveryTokenSaltedHash = newRcvrTokenSaltHash
// 		dbEnt.recoveryTokenSalt = newRcvrSalt

// 		if err := updateRow(ctx, tx, dbEnt); err != nil {
// 			return nil, err
// 		}
// 		return newRcvrToken, nil
// 	})
// 	if err != nil {
// 		return nil, err
// 	}
// 	return tbci.([]byte), nil

// }

// func createRecoveryTokenRecovery(ctx context.Context, resource string, recoveryToken []byte, newPassword string) error {
// 	resPepperHash := seasonStringHash(pepper, resource)

// 	newPwdSalt := make([]byte, 64)
// 	if _, err := rand.Read(newPwdSalt); err != nil {
// 		return err
// 	}
// 	newPwdSaltHash := seasonStringHash(newPwdSalt, newPassword)

// 	return ms.OnTx(ctx, defaultTransactionTimeoutsInMs*time.Millisecond, db, sql.LevelReadCommitted, false, func(tx *sql.Tx) error {
// 		dbEnt, err := selectRow(ctx, tx, resPepperHash)
// 		if err == sql.ErrNoRows {
// 			return errResourceNotFound
// 		}
// 		if err != nil {
// 			return err
// 		}

// 		rcvrTokenSaltHash := seasonHash(dbEnt.recoveryTokenSalt, recoveryToken)
// 		if !bytes.Equal(dbEnt.recoveryTokenSaltedHash, rcvrTokenSaltHash) {
// 			return errRecoveryTokenMismatch
// 		}

// 		dbEnt.passwordSaltedHash = newPwdSaltHash
// 		dbEnt.passwordSalt = newPwdSalt
// 		dbEnt.recoveryTokenSaltedHash = nil
// 		dbEnt.recoveryTokenSalt = nil

// 		if err := updateRow(ctx, tx, dbEnt); err != nil {
// 			return err
// 		}
// 		return nil
// 	})
// }

func seasonStringHash(season []byte, s string) []byte {
	return seasonHash(season, []byte(s))
}

func seasonHash(season []byte, value []byte) []byte {
	//Not necessary to check errors (They are inherited from io.Writer interface, but never return errors in sha512 package).
	s512 := sha512.New()
	s512.Write(season)
	s512.Write(value)
	return s512.Sum(nil)
}
