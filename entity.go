package main

type passwordEntity struct {
	resourcePepperedHash []byte
	passwordSaltedHash   []byte
	passwordSalt         []byte
}

// type recoveryTokenEntity struct {
// 	resourcePepperedHash    []byte
// 	recoveryTokenSaltedHash []byte
// 	recoveryTokenSalt       []byte
// }
